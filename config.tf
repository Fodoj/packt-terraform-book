terraform {
  backend "s3" {
    bucket = "packt-terraform"
    key = "mighty_trousers/terraform.tfstate"
    region = "eu-central-1"
    dynamodb_table = "packt_terraform_lock"
  }
}